from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


# List view of projects
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    context = {
        "project_list": projects,
    }
    return render(request, "projects/list.html", context)


# show project and its views
@login_required
def show_project(request, id):
    # need to get a link from project to task
    # get the selected project
    projects = Project.objects.get(id=id)
    tasks = projects.tasks.all()
    context = {
        "task_list": tasks,
        "project_list": projects,
    }
    return render(request, "projects/detail.html", context)


# create project - allows user to create a new project.
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
